package shared

import (
	"fmt"
	"github.com/lib/pq"
	"go.uber.org/zap"
	sqltrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/database/sql"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
	"log"
)

var BD *gorm.DB
var err error

func Init() {
	dbinfo := "sqlsrv:server=127.0.0.1;database=NIIF;user=sa;password=Omnicom2022*;port=1433;sslmode=disable"

	fmt.Println(dbinfo)

	sqltrace.Register("sqlsrv", &pq.Driver{}, sqltrace.WithServiceName("dbo"))

	BD, err = gorm.Open(sqlserver.Open(dbinfo), &gorm.Config{})
	if err != nil {
		zap.S().Info(err)
		zap.S().Panic(err)
		panic(err)
	}
}

func GetDb() *gorm.DB {
	return BD
}

func CloseDb() {
	sqlDB, err := BD.DB()
	if err != nil {
		log.Fatalln(err)
	}
	sqlDB.Close()
}
