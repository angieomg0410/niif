package controller

import (
	"github.com/gin-gonic/gin"
	"niff/services"
	"niff/shared"
)

func GetProvider(c *gin.Context) {
	response := shared.Context{}

	providers := services.GetProviders()

	c.JSON(200, response.ResponseData(shared.StatusSuccess, "", providers))
}

func CreateProvider(c *gin.Context) {
	response := shared.Context{}

}
