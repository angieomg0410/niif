package model

import (
	"gorm.io/gorm"
	"niff/shared"
)

type Providers struct {
	gorm.Model
	Nit          string `json:"nit" db:"nit" `
	BusinessName string `json:"business_name" db:"business_name"`
}

type UserRequest struct {
	Name               string `json:"name"`
	Surname            string `json:"surname"`
	SecondSurname      string `json:"second_surname"`
	CountryName        string `json:"country_name"`
	TypeIdentification string `json:"type_identification"`
	Credential         string `json:"credential"`
	Email              string `json:"email"`
}

func GetAllModel() []Providers {
	var providers []Providers

	shared.GetDb().Find(&providers)

	return providers
}
