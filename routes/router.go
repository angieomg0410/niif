package routes

import (
	"github.com/gin-gonic/gin"
	"niff/controller"
)

func Init() *gin.Engine {
	r := gin.New()

	Routes(r)

	return r
}

func Routes(r *gin.Engine) {
	v1 := r.Group("/v1")
	{
		provider := v1.Group("provider")
		provider.GET("", controller.GetProvider)
		provider.POST("", controller.CreateProvider)

	}
}
