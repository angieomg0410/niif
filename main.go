package main

import (
	"niff/model"
	"niff/routes"
	database "niff/shared"
)

func main() {
	r := routes.Init()

	database.Init()
	defer database.CloseDb()

	database.BD.AutoMigrate(&model.Providers{})

	r.Run(":3000")
}
